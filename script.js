
function getLevelName (numeroVitorias) {
    if (numeroVitorias <= 10) {
        return "Ferro"
    } else if ((10 < numeroVitorias) && (numeroVitorias <= 20)) {
        return "Bronze"
    } else if ((20 < numeroVitorias) && (numeroVitorias <= 50)) {
        return "Prata"
    } else if ((50 < numeroVitorias) && (numeroVitorias <= 80)) {
        return "Ouro"
    } else if ((80 < numeroVitorias) && (numeroVitorias <= 90)) {
        return "Diamante"
    } else if ((90 < numeroVitorias) && (numeroVitorias <= 100)) {
        return "Lendário"
    } else if (numeroVitorias > 100) {
        return "Imortal"
    } else {
        return "Não foi possível calcular o ranking"
    }
}

let vitorias = prompt()

console.log(getLevelName(vitorias))
